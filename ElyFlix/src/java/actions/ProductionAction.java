package actions;
import com.opensymphony.xwork2.ActionSupport;
import entity.Production;
import entity.ProductionDAO;
import java.util.List;

public class ProductionAction extends ActionSupport {
    
    private String mensagem;
    private Production production;
    private List<Production> productions;
    
    //Getters e Setters
    
    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Production getProduction() {
        return production;
    }

    public void setProduction(Production production) {
        this.production = production;
    }

    public List<Production> getProductions() {
        return productions;
    }

    public void setProductions(List<Production> productions) {
        this.productions = productions;
    }
    
    // Métodos
    
    public String add() {
        ProductionDAO productionDAO = new ProductionDAO();
        try {
            productionDAO.add(production);
        } catch (Exception ex) {
            setMensagem("Erro ao adicionar categoria");
            return "fracasso";
        }
        setMensagem("Categoria adicionada");
        return "sucesso";
    }
    
    public String get() {
        ProductionDAO productionDAO = new ProductionDAO();
        try {
            production = productionDAO.get(production.getId());
        } catch (Exception ex) {
            setMensagem("Erro ao obter a categoria" + ex.getMessage());
            return "fracasso";
        }
        return "sucesso";
    }
    
    public String update() {
        ProductionDAO productionDAO = new ProductionDAO();
        try {
            productionDAO.update(production);
        } catch (Exception ex) {
            setMensagem("Erro ao atualizar a categoria");
            return "fracasso";
        }
        setMensagem("Categoria atualizada com sucesso");
        return "sucesso";
    }
    
    public String getAll() {
        ProductionDAO productionDAO = new ProductionDAO();
        try {
            productions = productionDAO.getAll();
        } catch (Exception ex) {
            setMensagem("Erro ao obter a(s) categoria(s):"+ex.getMessage());
            return "fracasso";
        }
        return "sucesso";
    }
    
    public String remove() {
        ProductionDAO productionDAO = new ProductionDAO();
        try {
            productionDAO.remove(production.getId());
        } catch (Exception ex) {
            setMensagem("Erro ao remover a categoria");
            return "fracasso";
        }
        setMensagem("Categoria removida com sucesso");
        return "sucesso";
    }
    
}