package actions;
import com.opensymphony.xwork2.ActionSupport;
import entity.Category;
import entity.CategoryDAO;
import java.util.List;

public class CategoryAction extends ActionSupport {
    
    private String mensagem;
    private Category category;
    private List<Category> categories;
    
    //Getters e Setters
    
    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    
    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
    
    // Métodos
    
    public String add() {
        CategoryDAO categoryDAO = new CategoryDAO();
        try {
            categoryDAO.add(category);
        } catch (Exception ex) {
            setMensagem("Erro ao adicionar categoria");
            return "fracasso";
        }
        setMensagem("Categoria adicionada");
        return "sucesso";
    }
    
    public String get() {
        CategoryDAO categoryDAO = new CategoryDAO();
        try {
            category = categoryDAO.get(category.getId());
        } catch (Exception ex) {
            setMensagem("Erro ao obter a categoria" + ex.getMessage());
            return "fracasso";
        }
        return "sucesso";
    }
    
    public String update() {
        CategoryDAO categoryDAO = new CategoryDAO();
        try {
            categoryDAO.update(category);
        } catch (Exception ex) {
            setMensagem("Erro ao atualizar a categoria");
            return "fracasso";
        }
        setMensagem("Categoria atualizada com sucesso");
        return "sucesso";
    }
    
    public String getAll() {
        CategoryDAO categoryDAO = new CategoryDAO();
        try {
            categories = categoryDAO.getAll();
        } catch (Exception ex) {
            setMensagem("Erro ao obter a(s) categoria(s):"+ex.getMessage());
            return "fracasso";
        }
        return "sucesso";
    }
    
    public String remove() {
        CategoryDAO categoryDAO = new CategoryDAO();
        try {
            categoryDAO.remove(category.getId());
        } catch (Exception ex) {
            setMensagem("Erro ao remover a categoria");
            return "fracasso";
        }
        setMensagem("Categoria removida com sucesso");
        return "sucesso";
    }
    
}
