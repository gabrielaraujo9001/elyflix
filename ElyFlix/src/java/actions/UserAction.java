package actions;
import com.opensymphony.xwork2.ActionSupport;
import entity.User;
import entity.UserDAO;
import java.util.List;

public class UserAction extends ActionSupport {
    
    private String mensagem;
    private User user;
    private List<User> users;
    
    //Getters e Setters
    
    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
    
    // Métodos
    
    public String add() {
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.add(user);
        } catch (Exception ex) {
            setMensagem("Erro ao adicionar usuário: "+ex.getMessage());
            return "fracasso";
        }
        setMensagem("Usuário adicionado");
        return "sucesso";
    }
    
    public String get() {
        UserDAO userDAO = new UserDAO();
        try {
            user = userDAO.get(user.getId());
        } catch (Exception ex) {
            setMensagem("Erro ao obter o usuário" + ex.getMessage());
            return "fracasso";
        }
        return "sucesso";
    }
    
    public String update() {
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.update(user);
        } catch (Exception ex) {
            setMensagem("Erro ao atualizar o usuário");
            return "fracasso";
        }
        setMensagem("Usuário atualizado com sucesso");
        return "sucesso";
    }
    
    public String getAll() {
        UserDAO userDAO = new UserDAO();
        try {
            users = userDAO.getAll();
        } catch (Exception ex) {
            setMensagem("Erro ao obter o(s) usuário(s):"+ex.getMessage());
            return "fracasso";
        }
        return "sucesso";
    }
    
    public String remove() {
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.remove(user.getId());
        } catch (Exception ex) {
            setMensagem("Erro ao remover o usuário");
            return "fracasso";
        }
        setMensagem("Usuário removido com sucesso");
        return "sucesso";
    }
    
}