package entity;

public class CategoryDAO extends DAO<Category> {

    public CategoryDAO() {
        super(Category.class);
    }

    /* Implementar demais métodos necessários para gerenciar a persistência de usuário */
}