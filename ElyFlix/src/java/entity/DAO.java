package entity;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DAO<E> {

    private Class<E> persistentClass;

    public DAO(Class<E> persistentClass) {
        this.persistentClass = persistentClass;
    }

    public void add(E entity) {
        Session session = util.HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(entity);
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            session.close();
        }
    }

    public void update(E entity) throws Exception {
        Session session = util.HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(entity);
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            session.close();
        }
    }

    public void remove(E entity) throws Exception {
        Session session = util.HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(entity);
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            session.close();
        }
    }

    public void remove(long id) throws Exception {
        E entity = get(id);
        remove(entity);
    }

    public List<E> getAll() throws Exception {
        List<E> result = null;
        Session session = util.HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            result = session.createQuery("from " + persistentClass.getName()).list();
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            session.close();
        }
        return result;
    }

    public E get(long id) throws Exception {
        E entity = null;
        Session session = util.HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            entity = session.get(persistentClass, id);
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            session.close();
        }
        return entity;
    }
}