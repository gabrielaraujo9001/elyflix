package entity;

public class UserDAO extends DAO<User> {

    public UserDAO() {
        super(User.class);
    }

    /* Implementar demais métodos necessários para gerenciar a persistência de usuário */
}