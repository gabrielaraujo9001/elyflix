<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>ElyFlix</title>
    </head>
    <body>
        
        <header>
            <div class="logo">
                <a href="http://localhost:8084/ElyFlix">
                    <img src="img/logo.svg" height="42" width="129"/>
                </a>
            </div>
            <h1 class="page-title">Início</h1>
        </header>
        
        <main>
            
            <div class="dashboard">
                
                <s:url action="GetCategories" var="categories"></s:url>
                <div class="item"><s:a href="%{categories}"><i class="fa fa-tags"></i> Cadastro de categorias</s:a></div>

                <s:url action="GetProductions" var="productions"></s:url>
                <div class="item"><s:a href="%{productions}"><i class="fa fa-film"></i> Cadastro de programas</s:a></div>
                
                <s:url action="GetUsers" var="users"></s:url>
                <div class="item"><s:a href="%{users}"><i class="fa fa-user"></i> Cadastro de usuários</s:a></div>
                
            </div>
                
        </main>
            
    </body>
</html>