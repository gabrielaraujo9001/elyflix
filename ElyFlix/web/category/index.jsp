<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>ElyFlix - Categorias</title>
    </head>
    
    <body>
        
        <header>
            <div class="logo">
                <a href="http://localhost:8084/ElyFlix">
                    <img src="img/logo.svg" height="42" width="129"/>
                </a>
            </div>
            <h1 class="page-title"><i class="fa fa-tags"></i> Categorias</h1>
        </header>
        
        <main>
            
            <s:if test="mensagem != null && mensagem.trim().length() > 0">
                <div class="feedback-message">
                    <p class="message"><s:property value="mensagem" /></p>
                </div>
            </s:if>
            
            <s:form action="AddCategory" method="post" cssClass="form">
                <div class="form-field">
                    <s:label value="Nome:" cssClass="label" for="category.name" />
                    <s:textfield name="category.name" placeholder="Ex: comédia" cssClass="field" />
                </div>
                <div class="form-field -submit">
                    <s:submit cssClass="field" value="Adicionar" />
                </div>
            </s:form>

            <s:if test="categories != null && categories.size() > 0">
                <table class="table categories">
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>&nbsp;</th>
                    </tr>
                    <s:iterator value="categories" var="cat">
                        <tr>
                            <td><s:property value="#cat.getId()" /></td>
                            <td>
                                <s:url action="GetCategory" var="getCategory">
                                    <s:param name="category.id"><s:property value="#cat.getId()" /></s:param>
                                </s:url>
                                <s:a href="%{getCategory}">
                                    <s:property value="#cat.getName()" />
                                </s:a>
                            </td>
                            <td>
                                <s:url action="RemoveCategory" var="removeCategory">
                                    <s:param name="category.id"><s:property value="#cat.getId()" /></s:param>
                                </s:url>
                                <s:a href="%{removeCategory}"><i class="fa fa-trash"></i></s:a>
                            </td>
                        </tr>
                    </s:iterator>
                </table>
            </s:if>
            
        </main>
    
    </body>
</html>