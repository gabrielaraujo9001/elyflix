<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>ElyFlix - Produções</title>
    </head>
    
    <body>
        
        <header>
            <div class="logo">
                <a href="http://localhost:8084/ElyFlix">
                    <img src="img/logo.svg" height="42" width="129"/>
                </a>
            </div>
            <h1 class="page-title"><i class="fa fa-tags"></i> Editar produção: <s:property value="production.name" /></h1>
        </header>
        
        <main>
            
            <s:if test="mensagem != null && mensagem.trim().length() > 0">
                <div class="feedback-message">
                    <p class="message"><s:property value="mensagem" /></p>
                </div>
            </s:if>
            
            <s:form action="UpdateProduction" method="post" cssClass="form">
                <div class="form-field">
                    <s:label value="Id" cssClass="label" for="production.id" />
                    <s:textfield cssClass="field" name="production.id" readonly="true" />
                </div>
                <div class="form-field">
                    <s:label value="Nome:" cssClass="label" for="production.name" />
                    <s:textfield name="production.name" placeholder="Nome" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Tipo:" cssClass="label" for="production.type" />
                    <s:textfield name="production.type" placeholder="Ex: Filme, série" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Duração (em minutos):" cssClass="label" for="production.duration" />
                    <s:textfield name="production.duration" type="number" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Ano:" cssClass="label" for="production.releaseYear" />
                    <s:textfield name="production.releaseYear" type="number" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Produtora:" cssClass="label" for="production.producer" />
                    <s:textfield name="production.producer" placeholder="Ex: Marvel" cssClass="field" />
                </div>
                <div class="form-field -submit">
                    <s:submit cssClass="field" value="Atualizar" />
                </div>
            </s:form>
            
        </main>
    
    </body>
</html>