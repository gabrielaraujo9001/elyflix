<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>ElyFlix - Produções</title>
    </head>
    
    <body>
        
        <header>
            <div class="logo">
                <a href="http://localhost:8084/ElyFlix">
                    <img src="img/logo.svg" height="42" width="129"/>
                </a>
            </div>
            <h1 class="page-title"><i class="fa fa-film"></i> Produções</h1>
        </header>
        
        <main>
            
            <s:if test="mensagem != null && mensagem.trim().length() > 0">
                <div class="feedback-message">
                    <p class="message"><s:property value="mensagem" /></p>
                </div>
            </s:if>
            
            <s:form action="AddProduction" method="post" cssClass="form">
                <div class="form-field">
                    <s:label value="Nome:" cssClass="label" for="production.name" />
                    <s:textfield name="production.name" placeholder="Ex: Toy Story" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Tipo:" cssClass="label" for="production.type" />
                    <s:textfield name="production.type" placeholder="Ex: Filme, série" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Duração (em minutos):" cssClass="label" for="production.duration" />
                    <s:textfield name="production.duration" type="number" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Ano:" cssClass="label" for="production.releaseYear" />
                    <s:textfield name="production.releaseYear" type="number" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Produtora:" cssClass="label" for="production.producer" />
                    <s:textfield name="production.producer" placeholder="Ex: Marvel" cssClass="field" />
                </div>
                <div class="form-field -submit">
                    <s:submit cssClass="field" value="Adicionar" />
                </div>
            </s:form>

            <s:if test="productions != null && productions.size() > 0">
                <table class="table productions">
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Duração (min)</th>
                        <th>Ano</th>
                        <th>Produtora</th>
                        <th>&nbsp;</th>
                    </tr>
                    <s:iterator value="productions" var="prod">
                        <tr>
                            <td><s:property value="#prod.getId()" /></td>
                            <td>
                                <s:url action="GetProduction" var="getProduction">
                                    <s:param name="production.id"><s:property value="#prod.getId()" /></s:param>
                                </s:url>
                                <s:a href="%{getProduction}">
                                    <s:property value="#prod.getName()" />
                                </s:a>
                            </td>
                            <td><s:property value="#prod.getType()" /></td>
                            <td><s:property value="#prod.getDuration()" /></td>
                            <td><s:property value="#prod.getReleaseYear()" /></td>
                            <td><s:property value="#prod.getProducer()" /></td>
                            <td>
                                <s:url action="RemoveProduction" var="removeProduction">
                                    <s:param name="production.id"><s:property value="#prod.getId()" /></s:param>
                                </s:url>
                                <s:a href="%{removeProduction}"><i class="fa fa-trash"></i></s:a>
                            </td>
                        </tr>
                    </s:iterator>
                </table>
            </s:if>
            
        </main>
    
    </body>
</html>