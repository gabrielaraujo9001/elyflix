<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>ElyFlix - Usuários</title>
    </head>
    
    <body>
        
        <header>
            <div class="logo">
                <a href="http://localhost:8084/ElyFlix">
                    <img src="img/logo.svg" height="42" width="129"/>
                </a>
            </div>
            <h1 class="page-title"><i class="fa fa-tags"></i> Editar usuário: <s:property value="user.name" /></h1>
        </header>
        
        <main>
            
            <s:if test="mensagem != null && mensagem.trim().length() > 0">
                <div class="feedback-message">
                    <p class="message"><s:property value="mensagem" /></p>
                </div>
            </s:if>
            
            <s:form action="UpdateUser" method="post" cssClass="form">
                <div class="form-field">
                    <s:label value="Id" cssClass="label" for="user.id" />
                    <s:textfield cssClass="field" name="user.id" readonly="true" />
                </div>
                <div class="form-field">
                    <s:label value="Nome:" cssClass="label" for="user.name" />
                    <s:textfield name="user.name" placeholder="Ex: Gabriel" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Sobrenome:" cssClass="label" for="user.lastname" />
                    <s:textfield name="user.lastname" placeholder="Ex: Araujo" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Email:" cssClass="label" for="user.email" />
                    <s:textfield name="user.email" type="email" placeholder="Ex: email@gmail.com" cssClass="field" />
                </div>
                <div class="form-field">
                    <s:label value="Senha:" cssClass="label" for="user.password" />
                    <s:textfield name="user.password" type="password" placeholder="****" cssClass="field" />
                </div>
                <div class="form-field -submit">
                    <s:submit cssClass="field" value="Atualizar" />
                </div>
            </s:form>
            
        </main>
    
    </body>
</html>